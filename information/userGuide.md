Recommended you have an adblocker like ublock origin installed, etc before using this plugin

This plugin is not available officially on the google play store because it'd probably violate the terms (adult content).

Install the plugin from this repo : https://developer.chrome.com/docs/extensions/mv3/getstarted/ you will be loading an unpacked extnesion.

Modify the `background.js` file to do a naive "search"  for a given JAV code on the select sites as specified in the source code. 

Make sure to reload the extension when making changes to the `background.js` file.

Type in `jav` then hit tab and then put in your JAV code then hit `enter` to "search" for it and open various tabs to sites trying to look for information or screens on that code. 

