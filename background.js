
chrome.omnibox.onInputEntered.addListener((code)=> {
   search(code);
});

function createJavScreensUrl(javCode){
    let javScreensUrl = JAV_SCREENS_BASE_URL + "images/" + javCode.toUpperCase() + ".jpg"
    return javScreensUrl;
}

function openSiteInNewTab(newUrl){
    let newURL = newUrl;//clown zone
    chrome.tabs.create({url:newURL});
}

// Tech debt make this return list of errors instead possibly if error cases determined, notification pattern or whatever
function checkIfValidCode(javCode){
    if (javCode.length ===0){
        //project error or do nothing or something
        return false;
    }
    return true;
}

function search(javCode){
    if (!checkIfValidCode((javCode))){
        return;
    }

    openSiteInNewTab(createNyaaSiSearchUrl(javCode));

    let javCodePrefix = javCode.substring(0, javCode.indexOf('-')); //lazy check for now
    if (!DONT_EXPECT_TO_FIND_ON_JAV_LIBRARY_CODE_PREFIXES.includes(javCodePrefix.toUpperCase()) && isCodeLettersThenNumbers(javCode)){
        openSiteInNewTab(createJavScreensUrl(javCode));
        openSiteInNewTab(createJavLibrarySearchUrl(javCode));
        openSiteInNewTab(createR18SearchUrl(javCode));
    }

    // Comment out sites you don't care for or add new sites you want to search to 

    //openSiteInNewTab(createRJapanesePorn2SearchUrl(javCode));
    //openSiteInNewTab(createRAsianPornIn15SecondsSearchUrl(javCode));
}

let DONT_EXPECT_TO_FIND_ON_JAV_LIBRARY_CODE_PREFIXES = ["FC2PPV", "FC2 PPV", "PONDO", "1PONDO", "CWP", "CWPBD", "HEYZO", "CARIBBEANCOM", "SIRO" ];
//possibly add SMD, SMBD to above?

//more codes https://www.d2pass.com/guest/?lang=en
//https://en.heydouga.com/monthly/channel/list_provider.html

function isCodeLettersThenNumbers(javCode){
    // TODO: yes there should be a way to write intelligent regex where the hyphen in the middle is either once or none, didn't find it off google yet
    let lettersHyphenNumbersRegex = /^[a-zA-z]+-[0-9]+$/i ;
    let lettersNumbersRegex = /^[a-zA-z]+[0-9]+$/i ;

    return javCode.match(lettersHyphenNumbersRegex) || javCode.match(lettersNumbersRegex) ;
}


function createNyaaSiSearchUrl(javCode){
    return NYAA_SI_SEARCH_BASE+javCode;
}

function createNyaaNetSearchUrl(javCode){
    return NYAA_NET_SEARCH_BASE+javCode;
}

function createJavLibrarySearchUrl(javCode){
    return JAV_LIBRARY_SEARCH_BASE+javCode;
}

function createSearchSubreddit(subreddit, javCode){
    return REDDIT_SEARCH_PREFIX + subreddit + REDDIT_SEARCH_MIDDLE + javCode+ REDDIT_SEARCH_SUFFIX;
}

function createRJapanesePorn2SearchUrl(javCode){
    return createSearchSubreddit(JAPANESE_PORN2_SUBREDDIT, javCode);
}

function createRAsianPornIn15SecondsSearchUrl(javCode){
    return createSearchSubreddit(ASIANPORN_15SEC_SUBREDDIT, javCode);
}

function createR18SearchUrl(javCode){
    return R18+javCode.replace('-','00');
}

// Below contains a list of sites, some useful, some not

const NYAA_SI_SEARCH_BASE = 'https://sukebei.nyaa.si/?f=0&c=0_0&q=';

const JAV_SCREENS_BASE_URL = 'https://javscreens.com/';
const JAV_LIBRARY_SEARCH_BASE='https://www.javlibrary.com/en/vl_searchbyid.php?keyword=';

const JAPANESE_PORN2_SUBREDDIT = 'JapanesePorn2';
const ASIANPORN_15SEC_SUBREDDIT = 'Asianpornin15seconds';//maybe not worth putting in this

//avsox

const REDDIT_SEARCH_PREFIX = 'https://old.reddit.com/r/';
const REDDIT_SEARCH_MIDDLE = '/search?q=';
const REDDIT_SEARCH_SUFFIX='&restrict_sr=on&include_over_18=on';

const R18 = "https://r18.com/videos/vod/movies/detail/-/id=";
//r18 searching like this is not reliable regrettably

const NYAA_NET_SEARCH_BASE = 'https://sukebei.nyaa.net/search?c=_&q=';

